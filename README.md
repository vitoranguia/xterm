# Xterm 

My simple configuration [Xterm](https://invisible-island.net/xterm/)

![Print screen](https://pixelfed.social/storage/m/113a3e2124a33b1f5511e531953f5ee48456e0c7/85c26953a179a0e533565ce79e82deb9724813d5/ho0VcUfkW79B48ndb0QblhYrB7GzyUM0Wgy8EvNu.png)

Install

```
# apt install -y xterm
```

```
$ cp .Xresources "$HOME"/
```


Set as default

```
# update-alternatives --config x-terminal-emulator
```

Load the resource file

```
$ xrdb "$HOME"/.Xresources
```


Shortcuts

<kbd>Shift</kbd> + <kbd>PageUp</kbd> scroll up

<kbd>Shift</kbd> + <kbd>PageDown</kbd> scroll down

<kbd>Shift</kbd> + <kbd>Insert</kbd> paste (PRIMARY selection)

<kbd>Alt</kbd> + <kbd>Enter</kbd> full screen

Tested on xterm 344 and Debian 10
